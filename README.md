# clone-hero-server 🎸 🥁 🐳

Docker image for Clone Hero dedicated server software. Available on [Docker Hub](https://hub.docker.com/r/cfunseth/clone-hero-server).

The Docker image exposes port 14242 for network communication by default.

Settings for the server are defined as enviornmental variables. The ones that can be defined and/or modified include:
- NAME (name of server that gets displayed in Clone Hero)
- PASSWORD (password to enter in Clone Hero)
- IP (address you'd like the server to listen on, defaults to 0.0.0.0)

```$ docker run --rm -p 14242:14242/udp --env NAME=clone-hero-server --env PASSWORD=password cfunseth/clone-hero-server:latest```
