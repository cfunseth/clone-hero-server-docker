FROM debian:buster-slim

RUN apt-get update \
 && apt-get install --no-install-recommends -y ca-certificates libicu63 libgssapi-krb5-2 \
 && rm -rf /var/lib/apt/lists/* \
 && ln -sf /usr/src/clonehero/Server /usr/bin/cloneheroserver \
 && useradd -m clonehero

WORKDIR /usr/src/clonehero/
COPY Server ./
COPY startup.sh ./

RUN chmod +x ./Server \
 && chown -R 1000 ./

USER clonehero

ENV NAME='clone-hero-server'
ENV PASSWORD=''
ENV IP=''
ENV PORT=''

EXPOSE 14242/udp
ENTRYPOINT ["./startup.sh"]
